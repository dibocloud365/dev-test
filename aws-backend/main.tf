############################################################
# AFTER RUNNING TERRAFORM APPLY (WITH LOCAL BACKEND)
# YOU WILL UNCOMMENT THIS CODE THEN RERUN TERRAFORM INIT
# TO SWITCH FROM LOCAL BACKEND TO REMOTE AWS BACKEND
############################################################

terraform {
  backend "s3" {
  bucket         = "dibo-directive-tf-state" # REPLACE WITH YOUR BUCKET NAME
  key            = "devops/dibo-terraform.tfstate"
  region         = "us-east-1"
  dynamodb_table = "terraform-state-locking"
  encrypt        = true
  }
}

# terraform {
#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = "~> 3.0"
#     }
#   }
# }

# provider "aws" {
#   region = "us-east-1"
# }

# resource "aws_s3_bucket" "terraform_state" {
#   bucket        = "dibo-directive-tf-state" # REPLACE WITH YOUR BUCKET NAME
#   force_destroy = true
#   versioning {
#     enabled = true
#   }

#   server_side_encryption_configuration {
#     rule {
#       apply_server_side_encryption_by_default {
#         sse_algorithm = "AES256"
#       }
#     }
#   }
# }

# resource "aws_dynamodb_table" "terraform_locks" {
#   name         = "terraform-state-locking"
#   billing_mode = "PAY_PER_REQUEST"
#   hash_key     = "LockID"
#   attribute {
#     name = "LockID"
#     type = "S"
#   }
# }
